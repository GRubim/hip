
function Hip(virtual_width, virtual_height, gridx, gridy, bits) {
	function insertinfo(imageData, info, bits, color_index, precision) {
		var data = imageData.data;
		var sum = 0;
		var count = 0;
		for (var i=color_index; i<data.length; i+=4, ++count)
			sum += data[i];
		var current_med = sum/count;
		var mask = (~0)<<bits;
		var intended_med = Math.round(current_med)&mask|(info&(~mask));
		var sign = intended_med>current_med?1:-1;
		var remaining = (intended_med - current_med) * count;
		var width = imageData.width;
		var height = imageData.height;
		function rand() {
			var x = Math.random();
			x = Math.asin(x*2-1)/Math.PI + 0.5;
			return x;
		}
		while (Math.abs(remaining)>0.5) {
			var y = Math.floor(rand()*height);
			var x = Math.floor(rand()*width);
			var i = (y*width + x)*4 + color_index;
			var color = data[i] + sign;
			if (color>=0 && color<=0xff) {
				data[i] = color;
				remaining -= sign;
			}
		}
	}
	function readinfo(imageData, bits, color_index) {
		var data = imageData.data;
		var sum = 0;
		var count = 0;
		for (var i=color_index; i<data.length; i+=4, ++count)
			sum += data[i];
		var med = sum/count;
		var round_med = Math.round(med);
		var mask = (~0)<<bits;
		return {
			info: round_med&(~mask),
			variation: Math.abs(round_med-med)
		};
	}
	this.insert = function(image, info, precision) {
		var canvas = document.createElement("canvas");
		canvas.width = virtual_width;
		canvas.height = virtual_height;
		var image_width = parseInt(image.width);
		var image_height = parseInt(image.height);
		var context = canvas.getContext("2d");
		context.drawImage(image, 0, 0, image_width, image_height, 0, 0, virtual_width,
			virtual_height);
		var col = virtual_width/gridx;
		var row = virtual_height/gridy;
		var cell_sx = Math.floor(col);
		var cell_sy = Math.floor(row);
		var i, j, k, c, x, y, imageData;
		for (i=c=0; i<gridy; ++i) {
			for (j=0; j<gridx; ++j) {
				x = Math.floor(col*j);
				y = Math.floor(row*i);
				imageData = context.getImageData(x, y, cell_sx, cell_sy);
				for (k=0; k<3; ++k) {
					insertinfo(imageData, info[c++], bits, k, precision);
					var result = readinfo(imageData, bits, k);
				}
				context.putImageData(imageData, x, y);
			}
		}
		return canvas.toDataURL();
	};
	this.extract = function(image, info) {
		var canvas = document.createElement("canvas");
		canvas.width = virtual_width;
		canvas.height = virtual_height;
		var image_width = parseInt(image.width);
		var image_height = parseInt(image.height);
		var context = canvas.getContext("2d");
		context.drawImage(image, 0, 0, image_width, image_height, 0, 0, virtual_width,
			virtual_height);
		var col = virtual_width/gridx;
		var row = virtual_height/gridy;
		var cell_sx = Math.floor(col);
		var cell_sy = Math.floor(row);
		var i, j, k, c, x, y, imageData, result;
		var sum = 0;
		for (i=c=0; i<gridy; ++i) {
			for (j=0; j<gridx; ++j) {
				x = Math.floor(col*j);
				y = Math.floor(row*i);
				imageData = context.getImageData(x, y, cell_sx, cell_sy);
				for (k=0; k<3; ++k) {
					result = readinfo(imageData, bits, k);
					info[c++] = result.info;
					sum += result.variation;
				}
			}
		}
		info.splice(c, info.length - c);
		return 1 - (sum/c)*4;
	};
}
