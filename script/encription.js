
function Encription(salt_length, counter_length, hash_length, confirm, min_length, max_length) {
	if (salt_length==null)
		salt_length = 4;
	if (counter_length==null)
		counter_length = 4;
	if (hash_length==null)
		hash_length = 16;
	if (confirm==null)
		confirm = [];
	var pwd = "";
	var salt = [];
	var key = [];
	var hash_context = new Hashing(hash_length);
	var header_length = salt_length + confirm.length;
	function generateSalt() {
		var byte;
		salt.splice(0, salt.length);
		for (var i=salt_length; i--; salt.push(byte)) {
			byte = Math.floor(Math.random()*0x100);
		}
	}
	function getSalt(array) {
		salt.splice(0, salt.length);
		for (var i=0; i<salt_length; ++i) {
			salt.push(array[i]);
		}
	}
	function generateKey() {
		key.splice(0, key.length);
		for (var i=0; i<salt_length; ++i) {
			key.push(salt[i]);
		}
		for (var i=0; i<counter_length; ++i) {
			key.push(0);
		}
		for (var i=0; i<pwd.length; ++i) {
			key.push(pwd.charCodeAt(i));
		}
	}
	var buffer;
	var buf_count;
	var key_count;
	function initHashing() {
		generateKey();
		hash_context.init();
		hash_context.readBytes(key);
		buffer = [];
		buf_count = 0;
		key_count = 0;
	}
	function nextKeyByte() {
		if (key_count>=key.length) {
			var i, j, byte;
			for(i=byte=0, j=salt_length; i<counter_length && byte==0; ++i) {
				byte = key[i+j] = (key[i+j]+1)&0xff;
			}
			key_count = 0;
		}
		return key[key_count++];
	}
	function nextHashByte() {
		if (buf_count >= buffer.length) {
			var byte = nextKeyByte();
			hash_context.readByte(byte).updateOutput().getOutput(buffer);
			buf_count = 0;
		}
		return buffer[buf_count++];
	}
	this.setPassword = function(password) {
		pwd = password;
	};
	this.encrypt = function(input) {
		generateSalt();
		initHashing();
		var i;
		var output = [];
		for (i=0; i<salt_length; ++i) {
			output.push(salt[i]);
		}
		for (i=0; i<confirm.length; ++i) {
			output.push(nextHashByte()^confirm[i]);
		}
		var length = input.length;
		if (max_length!=null) {
			var exceeded = length + header_length - max_length;
			if (exceeded>0) {
				length -= exceeded;
			}
		}
		for (i=0; i<length; ++i) {
			output.push(nextHashByte()^input[i]);
		}
		if (min_length!=null) {
			var lacking = min_length - length - header_length;
			for (;lacking>0;--lacking)
				output.push(nextHashByte());
		}
		return output;
	};
	this.decrypt = function(input) {
		getSalt(input);
		initHashing();
		var i, j, byte;
		for (j=salt_length, i=0; i<confirm.length; ++i) {
			byte = input[i+j]^nextHashByte();
			if (byte!=confirm[i]) return false;
		}
		var output = [];
		j = salt_length + confirm.length;
		for (i=j; i<input.length; ++i) {
			output.push(input[i]^nextHashByte());
		}
		return output;
	};
}
