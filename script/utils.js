function splitBytes(array, current_length, new_length, preserve_tail) {
	var fill = {
		zeros: (function(){
			var array = [];
			var buffer = "";
			for (var i=0; i<256; ++i) {
				array[i] = buffer;
				buffer += "0";
			}
			return array;
		})(),
		left: function(number, length) {
			number = number.toString();
			var zeros = this.zeros[length - number.length] || "";
			return zeros + number;
		},
		right: function(number, length) {
			number = number.toString();
			var zeros = this.zeros[length - number.length] || "";
			return number + zeros;
		},
	};
	var buffer = "";
	var byte;
	var result = [];
	for (var i=0; i<array.length; ++i) {
		byte = array[i].toString(2);
		buffer += fill.left(byte, current_length);
		while (buffer.length>=new_length) {
			byte = buffer.substr(0, new_length);
			buffer = buffer.substr(new_length);
			result.push(parseInt(byte, 2));
		}
	}
	if (buffer.length && preserve_tail) {
		byte = fill.right(buffer, new_length);
		result.push(parseInt(byte, 2));
	}
	return result;
}

function stringToArray(string) {
	var array = [];
	var byte;
	for (var i=0; i<string.length; ++i) {
		byte = string.charCodeAt(i);
		if (byte>0xff) {
			if (byte<=0xffff) {
				array.push(0);
				array.push(byte>>8);
				array.push(byte&0xff);
			}
		} else if (byte==0) {
			array.push(0);
			array.push(0);
		} else {
			array.push(byte);
		}
	}
	return array;
}

function arrayToString(array) {
	var string = "";
	var byte;
	for (var i=0; i<array.length; ++i) {
		byte = array[i];
		if (byte==0) {
			byte = array[++i];
			if (byte>0) {
				byte = (byte<<8)|array[++i];
			}
		}
		string += String.fromCharCode(byte);
	}
	return string;
}

function removeStringVoid(string) {
	var zero = String.fromCharCode(0);
	var i = string.length;
	while (i>0 && string[i-1]==zero) --i;
	return string.substr(0, i);
}
