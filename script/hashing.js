
function Hashing(hash_length) {
	var N = hash_length;
	var matrix;
	var primes;
	var counter;
	var output;
	var L = N - 1;
	var N2 = N + N;
	function mergeint(x) {
		return (x&0xFFFF)^(x>>>16);
	}
	function reverseBit(x) {
		var bit, r = 0;
		for (var i=0; i<32; ++i) {
			bit = (x&(1<<i))>>>i;
			r = (r<<1)|bit;
		}
		return r;
	}
	function isPrime(x) {
		if (x<2) return false;
		if (x==2) return true;
		if (x%2==0) return false;
		var a = Math.sqrt(x), b = 3;
		for (;b<=a;b+=2)
			if (x%b==0) return false;
		return true;
	}
	function initPrimes() {
		var i;
		primes = [];
		var lastPrime = N;
		function nextPrime() {
			while (!isPrime(++lastPrime));
			return lastPrime;
		}
		for (i=0; i<N2; primes[i++]=nextPrime());
	}
	function initMatrix() {
		var a, c, i, j, k, l, p, u;
		matrix = [];
		for (i=0; i<N; ++i) {
			matrix[i] = [];
			for (j=0; j<N; matrix[i][j++]=0);
		}
		for (i=0; i<N; matrix[0][i] = primes[i++]&0xFFFF);
		for (i=0; i<N; matrix[L][i] = primes[N+(i++)]&0xFFFF);
		for (k=2; k--;)
			for (i=0; i<N; ++i)
				for (j=0; j<N; ++j) {
					u = matrix[(i+L)%N][j];
					l = matrix[i][(j+1)%N];
					a = mergeint(reverseBit(u))*mergeint(reverseBit(~l));
					matrix[(i+1)%N][j] = a ^ matrix[i][j];
				}
	}
	function initOutput() {
		output = [];
	}
	function initCounter() {
		counter = 0;
	}
	this.init = function() {
		initPrimes();
		initMatrix();
		initOutput();
		initCounter();
		return this;
	};
	this.readByte = function (byte) {
		var i, j, a, b;
		byte = byte&0xFF;
		++counter;
		for (i=0; i<N; ++i) {
			r = matrix[i][(i+1)%N];
			l = matrix[i][(i+L)%N];
			a = matrix[i][i];
			matrix[i][i] = (~a)^(((mergeint(r)+counter)&0xFFFF)*((mergeint(l)-counter)&0xFFFF));
		}
		for (i=0; i<N; ++i)
			for (j=0; j<N; ++j) {
				a = matrix[(i+1)%N][j]^byte;
				b = matrix[i][(j+L)%N]^counter;
				matrix[i][j] ^= (mergeint(a)*mergeint(b))^primes[(b^a)%primes.length];
			}
		return this;
	}
	this.readString = function(string) {
		for (var i=0; i<string.length; ++i)
			this.readByte(string.charCodeAt(i));
		return this;
	};
	this.readBytes = function(array) {
		for (var i=0; i<array.length; ++i)
			this.readByte(array[i]);
		return this;
	};
	this.updateOutput = function() {
		var i, j, v, a, b, c, p;
		for (i=0; i<N; ++i) {
			v = counter;
			for (j=0; j<N; ++j) {
				a = matrix[i][j]^(~counter);
				b = matrix[j][(i+L)%N]^counter;
				c = matrix[(j+L)%N][i]^(-counter);
				p = primes[mergeint(b)%primes.length];
				v ^= (mergeint(v)*mergeint(~(counter+matrix[j][i]))) ^ (c%p);
			}
			output[i] = v;
		}
		return this;
	};
	this.getOutput = function(array) {
		if (!array) array = [];
		var shift = matrix[0][0]&3;
		for (var i=0; i<N; ++i)
			array[i] = (output[i]>>(shift<<3))&0xFF;
		return array;
	};
	this.getHexOutput = function(array) {
		array = this.getOutput(array);
		var s = "";
		function hexbyte(x) {
			x = x&0xFF;
			return (x>>4).toString(16) + (x&0xF).toString(16);
		}
		for (var i=0; i<array.length; ++i)
			s += hexbyte(array[i]);
		return s;
	};
	this.getStringOutput = function(array) {
		if (!array) array = [];
		var shift = matrix[0][0]&3;
		var s = "", byte;
		for (var i=0; i<N; ++i) {
			byte = (output[i]>>(shift<<3))&0xFF;
			array[i] = byte;
			s += String.fromCharCode(byte);
		}
		return s;
	};
	this.hash = function(string) {
		var hash = "";
		this.init();
		for (var i=0; i<string.length; this.readByte(string.charCodeAt(i++)));
		this.updateOutput();
		return this.getHexOutput();
	};
	this.init();
}
