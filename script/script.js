
var setup = {
	gridx: 10,
	gridy: 10,
	bits: 1,
	max_area: 760000,
	password: null,
	precision: 1
};

var images = new Array(2);

function removeClass(element, className) {
	var att = element.getAttribute("class");
	if (!att) return false;
	att = att.split(" ");
	var index = att.indexOf(className);
	if (index==-1) return false;
	att.splice(index, 1);
	element.setAttribute("class", att.join(" "));
	return true;
}

function toNumber(x) {
	return parseInt((x.toString()).replace("px", ""));
}

function resize(image, width, height) {
	var img_width = toNumber(image.width);
	var img_height = toNumber(image.height);
	var canvas = document.createElement("canvas");
	canvas.width = width;
	canvas.height = height;
	var context = canvas.getContext("2d");
	context.drawImage(image, 0, 0, img_width, img_height, 0, 0, width, height);
	image.src = canvas.toDataURL();
}

function addClass(element, className) {
	var att = element.getAttribute("class");
	if (!att) {
		element.setAttribute("class", className);
		return true;
	}
	att = att.split(" ");
	var index = att.indexOf(className);
	if (index>=0) {
		return false;
	}
	att.push(className);
	element.setAttribute("class", att.join(" "));
	return true;
}

function hasClass(element, className) {
	var att = element.getAttribute("class");
	if (!att) return false;
	var index = att.split(" ").indexOf(className);
	return index>=0;
}

function siblingsBefore(element) {
	var parent = element.parentElement;
	var siblings = parent.children;
	for (var i=0; i<siblings.length && siblings[i]!=element; ++i);
	return i;
}

function closest(element, querySelector) {
	while ((element=element.parentElement)!=null)
		if (element.matches(querySelector))
			return element;
	return element;
}

function drawAtCenter(canvas, image) {
	var csx = canvas.width;
	var csy = canvas.height;
	var cscale = csx/csy;
	var isx = toNumber(image.width);
	var isy = toNumber(image.height);
	var iscale = isx/isy;
	if (cscale > iscale) {
		isy = csy;
		isx = csy*iscale;
	} else {
		isx = csx;
		isy = csx/iscale;
	}
	var context = canvas.getContext("2d");
	context.clearRect(0, 0, csx, csy);
	context.drawImage(image, (csx-isx)/2, (csy-isy)/2, isx, isy);
}

function bindTabEvents() {
	var tabs = document.querySelectorAll("div.tabs div.tab");
	function getActivedTab() {
		return document.querySelector("div.tabs div.tab.actived");
	}
	var contents = document.querySelectorAll("div.tab-contents > div.tab-content");
	var i;
	for (i=0; i<tabs.length; ++i) {
		tabs[i].addEventListener("click", function(){
			if (hasClass(this, "actived")) return;
			var activedTab = getActivedTab();
			var index = siblingsBefore(activedTab);
			removeClass(activedTab, "actived");
			removeClass(contents[index], "actived");
			index = siblingsBefore(this);
			addClass(this, "actived");
			addClass(contents[index], "actived");
		});
	}
}

function linkFilesWithPreviewCanvas() {
	var allcanvas = document.querySelectorAll("div.tab-content canvas");
	var style;
	var canvas;
	for (i=0; i<allcanvas.length; ++i) {
		canvas = allcanvas[i];
		style = window.getComputedStyle(canvas);
		canvas.width = toNumber(style.width);
		canvas.height = toNumber(style.height);
	}
}

function getContentCanvas(element) {
	var content = closest(element, "div.tab-content");
	return content.querySelector("canvas");
}

function bindFileEvents() {
	function activateSecondButton(input) {
		var content = closest(input, "div.tab-content");
		var button = content.querySelector("div.button-line > button:nth-child(2)");
		removeClass(button, "disabled");
	}
	var allfiles = document.querySelectorAll("input[type=\"file\"]");
	for (var i=0; i<allfiles.length; ++i) {
		(function(file, index){
			file.addEventListener("change", function(){
				if (this.files.length==0) return;
				var input = this;
				var reader = new FileReader();
				reader.onload = function (event) {
					var preview = getContentCanvas(input);
					images[index] = document.createElement("img");
					var image = images[index];
					image.src = event.target.result;
					image.addEventListener("load", function(){
						if (index==0 && setup.max_area) {
							var area = toNumber(toNumber(image.width)*toNumber(image.height));
							if (area > setup.max_area) {
								var scale = Math.sqrt(setup.max_area/area);
								var new_width = Math.floor(image.width*scale);
								var new_height = Math.floor(image.height*scale);
								resize(image, new_width, new_height);
							}
						}
						drawAtCenter(preview, image);
					});
					activateSecondButton(input);
				};
				reader.readAsDataURL(this.files[0]);
			});
		})(allfiles[i], i);
	}
}

function getContentTextarea(element) {
	var content = closest(element, "div.tab-content");
	return content.querySelector("textarea");
}

function bindInsertButton() {
	var download = document.getElementById("download");
	document.getElementById("insert").addEventListener("click", function(){
		if (hasClass(this, "disabled")) return;
		var textarea = getContentTextarea(this);
		var width = toNumber(images[0].width);
		var height = toNumber(images[0].height);
		var hip = new Hip(width, height, setup.gridx, setup.gridy, setup.bits);
		var array = stringToArray(textarea.value);
		array = splitBytes(array, 8, setup.bits, true);
		var length = setup.gridx*setup.gridy*setup.bits*3;
		while (array.length < length)
			array.push(0);
		array.splice(length, array.length - length);
		var dataurl = hip.insert(images[0], array, setup.precision);
		var result = document.createElement("img");
		result.src = dataurl;
		var canvas = getContentCanvas(this);
		result.addEventListener("load", function(){
			drawAtCenter(canvas, result);
			download.href = result.src;
			var button = download.querySelector("button.disabled");
			if (button)
				removeClass(button, "disabled");
		});
	});
}

function bindExtractButton() {
	document.getElementById("extract").addEventListener("click", function(){
		if (hasClass(this, "disabled")) return;
		var width = toNumber(images[1].width);
		var height = toNumber(images[1].height);
		var hip = new Hip(width, height, setup.gridx, setup.gridy, setup.bits);
		var array = [];
		var precision = hip.extract(images[1], array);
		var textarea = getContentTextarea(this);
		array = splitBytes(array, setup.bits, 8, false);
		var text = arrayToString(array);
		textarea.value = removeStringVoid(text);
	});
}

window.addEventListener("load", function(){
	bindTabEvents();
	linkFilesWithPreviewCanvas();
	bindFileEvents();
	bindInsertButton();
	bindExtractButton();
});
